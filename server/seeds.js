const faker = require('faker')
const _ = require('lodash')
require('./index')
const User = require('./models/User')
const Post = require('./models/Post')
const Comment = require('./models/Comment')

const POSTS_TO_ADD = 2
let _id, email

const random = (min, max) => Math.floor(min + Math.random() * (max + 1 - min))

const createPost = async () => {

  const post = new Post({
    author: _id,
    title: faker.name.title(),
    content: faker.lorem.paragraphs(),
    image: faker.image.avatar(),
    comments: []
  })

  await post.save()

  createComments(post._id)

}

const createComments = async postId => {

  await Comment.create(
    _.times(random(1, 4), () => ({
      content: faker.lorem.sentences(),
      author: _id,
      postId
    }))
  )

  await createCommentsForComments(postId)

}

const createCommentsForComments = async postId => {

  const post = await Post.findById(postId).populate('comments')
  const { comments } = post

  await _.forEach(comments, async comment => {

    const replies = _.times(random(0, 3), () => ({
      content: faker.lorem.sentences(),
      author: { _id, email }
    }))

    await Comment.findOneAndUpdate({ _id: comment._id }, {
      $push: { replies }
    })

  })
}

(async () => {

  const user = await User.findOne()
  _id = user._id
  email = user.email

  Promise.all(
    _.times(POSTS_TO_ADD, async () => { await createPost() })
  ).then(() => console.log("//// SUCCESS ////"))

})()