const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const pug = require('pug')

const User = require('../models/User')

const keys = require('../config/keys')
const errorHandler = require('../utils/errorHandler')
const sendEmail = require('../utils/sendEmail')

const companyName = '«Company name»'
const mailOptionsEmailVerify = ({ email }, link) => ({
  subject: `${companyName} email verification`,
  html: pug.compileFile('templates/auth/verification.pug')({ email, link })
})
const mailOptionsPasswordReset = ({ email }, link) => ({
  subject: `${companyName} password reset`,
  html: pug.compileFile('templates/auth/passwordReset.pug')({ email, link })
})

// Basic
module.exports.login = async (req, res) => {
  const { email, password, socialId } = req.body

  const candidate = await User.findOne({ email })
  let compareResult

  if (candidate) {

    if (socialId)
      compareResult = bcrypt.compareSync(socialId, candidate.socialId)
    else
      compareResult = bcrypt.compareSync(password, candidate.password)

    if (compareResult) {
      const expiresIn = 604800 // one week
      const token = jwt.sign({ userId: candidate._id }, keys.JWT, { expiresIn })

      res.json({ success: true, token: `Bearer ${token}` })
    } else
      res.json({ success: false })

  } else
    res.json({ success: false })

}
module.exports.register = async (req, res) => {
  const { email, password, socialId } = req.body
  const { clienturl } = req.headers

  const candidate = await User.findOne({ email })

  if (candidate)
    res.json({ success: false, message: 'Such user already exists' })
  else {

    const salt = bcrypt.genSaltSync(10)
    let user

    if (socialId) user = new User({
      email,
      socialId: bcrypt.hashSync(socialId, salt)
    })
    else user = new User({
      email,
      password: bcrypt.hashSync(password, salt)
    })

    try {
      await user.save()

      await createVerifyEmail(user, clienturl)

      res.json({ success: true, user: user.getPublicProfile() })

    } catch (e) { errorHandler(res, e) }

  }
}
module.exports.user = async (req, res) => {
  const { id } = req.user

  try {
    const user = await User.findById(id)

    res.json({ success: true, user: user.getPublicProfile() })

  } catch (e) { errorHandler(res, e) }
}

// Verification
module.exports.sendVerifyEmail = async (req, res) => {
  const { id } = req.user
  const { clienturl } = req.headers

  try {

    const user = await User.findById(id)
    const { active, verificationHash } = user

    if (active)
      res.json({ success: false, message: 'User is already verified' })
    else {

      const link = `${clienturl}/auth/email-verification/${verificationHash}`
      const mailOptions = mailOptionsEmailVerify(user, link)
      await sendEmail(mailOptions)

      res.json({ success: true })
    }

  } catch (e) { errorHandler(res, e) }

}
module.exports.verify = async (req, res) => {
  const { hash } = req.query

  try {

    await User.findOneAndUpdate({ verificationHash: hash }, {
      $set: { active: true, verificationHash: null }
    })

    res.json({ success: true })

  } catch (e) { errorHandler(res, e) }

}

// Password reset
module.exports.sendResetPasswordEmail = async (req, res) => {
  const { clienturl } = req.headers
  const { email } = req.body

  try {

    const user = await User.findOne({ email })

    if (!user)
      return res.json({ success: false, message: 'No such user' })

    const hash = user.passwordResetHash || genHash()
    user.passwordResetHash = hash

    await user.save()

    const link = `${clienturl}/auth/password-reset/${hash}`
    const mailOptions = mailOptionsPasswordReset(user, link)
    await sendEmail(mailOptions)

    res.json({ success: true })

  } catch (e) { errorHandler(res, e) }

}
module.exports.resetPassword = async (req, res) => {
  const { hash, password } = req.body
  const salt = bcrypt.genSaltSync(10)

  try {

    await User.findOneAndUpdate({ passwordResetHash: hash }, {
      $set: {
        password: bcrypt.hashSync(password, salt),
        passwordResetHash: null
      }
    })

    res.json({ success: true })

  } catch (e) { errorHandler(res, e) }

}

async function createVerifyEmail(user, clienturl) {

  const hash = genHash()
  user.verificationHash = hash

  await user.save()

  const link = `${clienturl}/auth/email-verification/${hash}`
  const emailOptions = mailOptionsEmailVerify(user, link)
  await sendEmail(emailOptions)

}

function genHash() {
  return Math.random().toString(36).substring(2, 20) + Math.random().toString(36).substring(2, 20);
}