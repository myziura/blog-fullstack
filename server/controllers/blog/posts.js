const fs = require('fs')
const jimp = require('jimp')

const Post = require('../../models/Post')
const errorHandler = require('../../utils/errorHandler')

const saveImage = file => new Promise((resolve, reject) => {

  const { destination, filename } = file
  const path = `./${destination}${filename}`

  jimp.read(path).then(async image => {

    const newFilename = filename.replace(/(.png|.jpg|.jpeg|.gif|.webp)/, '')
    const newPath = `/${destination}${newFilename}.jpg`

    image.cover(1024, 500).quality(60).write(`.${newPath}`)

    if (fs.existsSync(path)) await fs.unlink(path)

    resolve(newPath)

  }).catch(error => reject(error))

})

module.exports.getAll = async (req, res) => {

  const { page, user, keyword, recent } = req.query
  let filters = {}

  const limit = 3

  if (user) filters['author'] = user

  // recent
  let sort = 'createdAt'
  if (recent === undefined || recent === 'true') sort = '-createdAt'

  if (keyword) filters['$or'] = [
    { title: { $regex: keyword, $options: 'i' } },
    { content: { $regex: keyword, $options: 'i' } }
  ]

  const skipPages = (+page || 1) - 1

  try {

    const pages = Math.ceil(await Post.find(filters).countDocuments() / limit)
    const posts = await Post.find(filters)
      .select('-comments')
      .populate('author')
      .limit(limit)
      .skip(limit * skipPages)
      .sort(sort)

    res.json({ success: true, posts, pages })

  } catch (e) { errorHandler(res, e) }

}
module.exports.getById = async (req, res) => {

  const { id } = req.params

  try {

    const post = await Post.findById(id)
      .populate('author', 'email')
      .populate({
        path: 'comments',
        populate: {
          path: 'author',
          select: 'email',
        }
      })
      .populate({
        path: 'comments.comments',
        populate: {
          path: 'author',
          select: 'email'
        }
      })

    if (post) res.json({ success: true, post })
    else res.json({ success: false })

  } catch (e) { errorHandler(res, e) }
}
module.exports.getByUserId = async (req, res) => {

  const { id } = req.params

  try {

    const posts = await Post.find({ 'author.id': id })

    res.json({ success: true, posts })

  } catch (e) { errorHandler(res, e) }
}

module.exports.create = async (req, res) => {

  const { _id, email } = req.user
  const { title, content } = req.body

  const image = await saveImage(req.file)

  const post = new Post({ author: _id, title, content, image })

  try {

    await post.save()

    res.json({ success: true, post })

  } catch (e) { errorHandler(res, e) }
}
module.exports.update = async (req, res) => {

  const { id } = req.params
  const userId = req.user.id
  const { title, content } = req.body
  let image

  if (req.file) {
    image = await saveImage(req.file)
  } else
    image = req.body.image

  try {

    const candidate = await Post.findOne({ _id: id })

    if (candidate.author._id == userId) {

      const post = await Post.findOneAndUpdate({ _id: id }, {
        $set: { image, title, content }
      })

      // Если загружаем новую картинку, то удаляем старую
      const path = `.${candidate.image}`
      if (fs.existsSync(path)) await fs.unlink(path)

      res.json({ success: true })

    } else
      res.json({ success: false, message: 'You have not such roots' })

  } catch (e) { errorHandler(res, e) }
}
module.exports.delete = async (req, res) => {

  const { id } = req.params
  const userId = req.user.id

  try {

    const post = await Post.findById(id)
    const { author, image } = post
    const path = `.${image}`

    if (author == userId) {

      if (fs.existsSync(path)) await fs.unlink(path)

      await post.remove()

      res.json({ success: true })

    } else
      res.json({ success: false, message: 'You have not such roots' })

  } catch (e) { errorHandler(res, e) }
}