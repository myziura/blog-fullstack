const Comment = require('../../models/Comment')
const Post = require('../../models/Post')
const errorHandler = require('../../utils/errorHandler')

module.exports.create = async (req, res) => {

  const { content, postId } = req.body
  const { id } = req.user

  try {

    const comment = new Comment({ content, postId, author: id })

    await comment.save()

    res.json({ success: true })

  } catch (e) { errorHandler(res, e) }
}
module.exports.update = async (req, res) => {

  const { id } = req.params
  const { content, reply, deleteId, updateId } = req.body

  try {

    if (reply)
      await Comment.findByIdAndUpdate(id, {
        $push: { replies: reply }
      })
    else if (deleteId) {
      await Comment.findByIdAndUpdate(id, {
        $pull: { replies: { _id: deleteId } }
      })
    } else if (updateId) {
      await Comment.findOneAndUpdate({
        _id: id,
        'replies._id': updateId
      }, {
        $set: { 'replies.$.content': content }
      })
    } else if (content && !updateId)
      await Comment.findByIdAndUpdate(id, { $set: { content } })

    res.json({ success: true })

  } catch (e) { errorHandler(res, e) }
}
module.exports.delete = async (req, res) => {

  const { id } = req.params
  const userId = req.user.id

  try {

    const comment = await Comment.findById(id)

    if (comment.author == userId) {

      await comment.remove()

    } else {
      res.json({ success: false, message: 'You have not such roots' })
    }

    res.json({ success: true })

  } catch (e) { errorHandler(res, e) }
}