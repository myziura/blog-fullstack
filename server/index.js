const fs = require('fs')
const http = require('http')
const https = require('https')
const keys = require('./config/keys')
const app = require('./app')

const port = keys.PORT

// Disabling
app.disable('x-powered-by')

if (process.env.NODE_ENV === 'production') {
  const options = {
    key: fs.readFileSync('../etc/letsencrypt/live/project-y.tk/privkey.pem'),
    cert: fs.readFileSync('../etc/letsencrypt/live/project-y.tk/fullchain.pem')
  }
  https.createServer(options, app).listen(port, () => {
    console.log(`-HTTPS server has been started on ${port}`)
  })
} else
  http.createServer(app).listen(port, () => {
    console.log(`-HTTP server has been started on ${port}`)
  })