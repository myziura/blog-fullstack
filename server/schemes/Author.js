const mongoose = require('mongoose')
const Schema = mongoose.Schema

const authorSchema = new Schema({
  _id: {
    ref: 'users',
    type: Schema.Types.ObjectId,
    required: true
  },
  email: {
    type: String,
    required: true,
    lowercase: true
  }
})

module.export = authorSchema