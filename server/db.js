const mongoose = require('mongoose')
const keys = require('./config/keys')

mongoose.connect(keys.MONGO_URI, { useCreateIndex: true, useNewUrlParser: true })
  .then(() => console.log('MongoDB connected'))
  .catch(e => console.error(e))