const nodemailer = require('nodemailer')

const keys = require('../config/keys')

module.exports = options => {

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'email.tester.misha@gmail.com',
      pass: keys.NODEMAILER_PASS
    }
  })

  const mailOptions = {
    from: 'email.tester.misha@gmail.com',
    to: 'ILikePoltava@gmail.com',
    ...options
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) console.log(error)
    else console.log('Email sent: ' + info.response)
  })
}