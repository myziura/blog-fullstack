module.exports = (res, error) => {
  res.json({
    success: false,
    message: error.message ? error.message : error
  })
}