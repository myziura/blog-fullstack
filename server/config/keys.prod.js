module.exports = {
  MONGO_URI: process.env.MONGO_URI,
  NODEMAILER_PASS: process.env.NODEMAILER_PASS,
  JWT: process.env.JWT
}