const ip = require('ip')

const general = {
  PORT: 4999,
  IP: ip.address(),
  DOMEN: 'project-y.tk'
}

if (process.env.NODE_ENV === 'production')
  module.exports = {
    ...general,
    ...require('./keys.prod')
  }
else
  module.exports = {
    ...general,
    ...require('./keys.dev')
  }