const express = require('express')
const app = express()
const passport = require('passport')
const bodyParser = require('body-parser')

const authRoutes = require('./routes/auth')

// MongoDB
require('./db')

// Plugins/Modules
app.use(passport.initialize())
require('./middleware/passport')(passport)
app.use(require('cors')({
  // origin: ['http://localhost:8080'],
  methods: ['GET', 'POST', 'PATCH', 'DELETE'],
  allowedHeaders: ['Content-Type', 'Authorization', 'clienturl']
}))
app.use(require('morgan')('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Routes
app.use('/uploads', express.static('uploads'))
app.use('/api/auth', authRoutes)
require('./routes/blog')(app)

module.exports = app