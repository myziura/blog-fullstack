const multer = require('multer')
const moment = require('moment')

const storage = multer.diskStorage({
  destination(req, file, cb) { cb(null, 'uploads/') },

  filename(req, file, cb) {
    const date = moment().format('YMMDD-HHmmss-SSS')
    const filename = `${date}.${file.mimetype.split('/')[1]}`

    cb(null, filename)
  }
})

const fileFilter = (req, file, cb) => {

  if (!file.originalname.match(/\.(jpg|jpeg|png|webp)$/)) {
    cb(new Error('Invalid file format. Use jpg/jpeg/png/webp'))
    return
  }

  cb(null, true)
}

// Size limit 5MB
const limits = { fileSize: 1024 * 1024 * 5 }

module.exports = multer({ storage, fileFilter, limits })