module.exports = (error, req, res, next) => {
  res.json({
    success: false,
    message: error.message ? error.message : error
  })
}