module.exports = app => {
  app.use('/api/blog-post', require('./posts'))
  app.use('/api/blog-comment', require('./comments'))
}