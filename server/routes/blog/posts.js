const express = require('express')
const passport = require('passport').authenticate('jwt', { session: false })
const router = express.Router()
const controller = require('../../controllers/blog/posts')
const upload = require('../../middleware/upload').single('image')
const errorHandler = require('../../middleware/errorHandler')

router.get('/', controller.getAll)
router.get('/:id', controller.getById)
router.get('/user/:id', controller.getByUserId)

router.post('/', upload, passport, controller.create, errorHandler)
router.patch('/:id', upload, passport, controller.update, errorHandler)
router.delete('/:id', passport, controller.delete)

module.exports = router