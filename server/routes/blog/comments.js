const express = require('express')
const passport = require('passport').authenticate('jwt', { session: false })
const router = express.Router()
const controller = require('../../controllers/blog/comments')

router.post('/', passport, controller.create)
router.patch('/:id', passport, controller.update)
router.delete('/:id', passport, controller.delete)

module.exports = router