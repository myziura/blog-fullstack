const express = require('express')
const controller = require('../controllers/auth')
const router = express.Router()
const passport = require('passport').authenticate('jwt', { session: false })

// Basic
router.post('/login', controller.login)
router.post('/register', controller.register)
router.get('/user', passport, controller.user)

// Verification
router.get('/send-verify-email', passport, controller.sendVerifyEmail)
router.get('/verify', controller.verify)

// Password reset
router.post('/send-reset-password-email', controller.sendResetPasswordEmail)
router.post('/reset-password', controller.resetPassword)

module.exports = router