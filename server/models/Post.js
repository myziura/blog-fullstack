const mongoose = require('mongoose')
const Schema = mongoose.Schema

const keys = require('../config/keys')

const postSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true,
  },
  content: {
    type: String,
    required: true,
    trim: true,
    maxlength: 3000,
  },
  image: {
    type: String,
    required: true,
    trim: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'comments'
  }]
}, {
  timestamps: true
})

postSchema.methods.toJSON = function () {
  const post = this.toObject()

  if (!post.image.includes('http')) {

    if (process.env.NODE_ENV === 'production')
      post.image = `//${keys.DOMEN}:${keys.PORT}${this.image}`
    else
      post.image = `//localhost:${keys.PORT}${this.image}`

  }

  // Make profile public
  delete post.__v

  delete post.author.__v
  delete post.author.password
  delete post.author.verificationHash
  delete post.author.passwordResetHash

  return post
}

postSchema.pre('remove', async function (next) {
  const Comment = mongoose.model('comments')

  await Comment.remove({ _id: { $in: this.comments } })

  next()
})

module.exports = mongoose.model('posts', postSchema)