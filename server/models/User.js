const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true,
    required: true
  },
  role: {
    type: String,
    default: 'user'
  },
  active: {
    type: Boolean,
    default: false
  },

  // Type
  password: {
    type: String
  },

  socialId: {
    type: String
  },

  // Secondary
  verificationHash: {
    type: String,
    default: null
  },

  passwordResetHash: {
    type: String,
    default: null
  }
})

userSchema.methods.getPublicProfile = function () {
  const user = this.toObject()

  delete user.__v
  delete user.password
  delete user.socialId
  delete user.verificationHash
  delete user.passwordResetHash

  return user
}

module.exports = mongoose.model('users', userSchema)