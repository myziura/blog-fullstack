const mongoose = require('mongoose')
const Schema = mongoose.Schema
const authorSchema = require('../schemes/Author')

const replySchema = new Schema({
  content: {
    type: String,
    trim: true,
    required: true
  },
  author: authorSchema,
  updatedAt: {
    type: Date,
    default: () => new Date().toISOString()
  }
})

const commentSchema = new Schema({
  content: {
    type: String,
    trim: true,
    required: true
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  postId: {
    type: Schema.Types.ObjectId,
    ref: 'posts'
  },
  replies: [replySchema]
}, { timestamps: true })

commentSchema.pre('save', async function (next) {
  const Post = mongoose.model('posts')

  await Post.findOneAndUpdate({ _id: this.postId }, {
    $push: { comments: { $each: [this._id], $position: 0 } }
  })

  next()
})

commentSchema.pre('remove', async function (next) {
  const Post = mongoose.model('posts')
  const post = await Post.findById(this.postId)

  const { comments } = post
  comments.splice(comments.indexOf(this._id), 1)

  await post.save()

  next()
})

module.exports = mongoose.model('comments', commentSchema)