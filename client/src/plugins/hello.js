import Vue from 'vue'
import hello from 'hellojs'

Vue.prototype.$hello = hello

hello.init({
  instargam: "df16da92489942e5b0a35322af13cec2",
  facebook: "715226142211716",
  google: "509287225155-pacbc5t0vp01ncttniqvgn72t4lafgao.apps.googleusercontent.com"
}, {
  redirect_uri: location.protocol + "//" + location.host
})