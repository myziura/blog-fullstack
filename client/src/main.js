import Vue from 'vue'
import axios from 'axios'

import Hello from '@/plugins/hello'
import Vuetify from '@/plugins/vuetify'
import VeeValidate from '@/plugins/vee-validate'

import App from '@/App'
import router from '@/router'
import store from '@/store/index'
import interceptors from '@/interceptors'

Vue.config.productionTip = false
Vue.prototype.$vueRouter = router

// axios.defaults.baseURL = 'https://project-y.tk:4999'
axios.defaults.baseURL = 'http://localhost:4999'

new Vue({
    router,
    store,
    render: h => h(App)
  })
  .$mount('#app')