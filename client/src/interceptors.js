import axios from 'axios'
import store from '@/store'
import * as types from '@/store/mutation-types'

const clientUrl = location.protocol + "//" + location.host

axios.interceptors.request.use(config => {

  store.commit(`auth/${types.SAVE_LOADING_STATE}`, true)

  config.headers = {
    ...config.headers,
    clientUrl,
    authorization: store.getters['auth/token'],
  }

  return config
}, error => {

  store.commit(`auth/${types.SAVE_LOADING_STATE}`, false)

  console.error(error)

  return Promise.reject(error)
})

axios.interceptors.response.use(response => {

  store.commit(`auth/${types.SAVE_LOADING_STATE}`, false)

  console.warn(response)

  return response
}, error => {

  store.commit(`auth/${types.SAVE_LOADING_STATE}`, false)

  console.error(error)
  if (error.response.status === 401) store.dispatch('auth/logout')

  return Promise.reject(error)
})