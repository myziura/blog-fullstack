export const UTILS_MIXINS_SOCIAL_AUTH = {
  methods: {

    onAuthLoginSuccess(user) { this.$emit('success', user) },

    onAuthLoginFailure(error) { this.$emit('error', error) },

    loginHello(social) {

      this.$hello(social).login({ scope: 'email' }).then(() => {

        this.$hello(social).api('me').then(response => {

          console.warn(response)

          this.onAuthLoginSuccess({
            // name: response.first_name,
            // surname: response.last_name,
            email: response.email,
            // password: response.id + response.email.split('@')[0],
            socialId: response.id,
            type: 'social'
          })
        })

      })
    },

    isLogin(social) {
      const session = this.$hello(social).getAuthResponse()
      const currentTime = (new Date()).getTime() / 1000

      return session && session.access_token && session.expires > currentTime;
    },

    authenticate(social) {
      if (this.isLogin(social))
        this.$hello(social).logout().then(() => this.loginHello(social))
      else
        this.loginHello(social)
    }

  }
}