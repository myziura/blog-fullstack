import Vue from 'vue'
import store from '@/store'

export default (to, from, next) => {

  const isAuthenticated = store.getters['auth/isAuthenticated']
  const decision = isAuth => isAuth ? next() : next({ name: 'login' })

  // null если запроса на получение юзера еще небыло
  if (isAuthenticated === null) {

    store.watch(() => store.getters['auth/isAuthenticated'], (value) => {
      decision(value)
    })

  } else decision(isAuthenticated)

}