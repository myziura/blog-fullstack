import Vue from 'vue'
import Router from 'vue-router'
import routerGuard from '@/utils/router-guard'

const LayoutAuth = () => import('@/layouts/LayoutAuth')
const AuthLogin = () => import('@/components/auth/AuthLogin')
const AuthSignup = () => import('@/components/auth/AuthSignup')
const AuthPasswordReset = () => import('@/components/auth/AuthPasswordReset')
const AuthForgotPassword = () => import('@/components/auth/AuthForgotPassword')
const AuthEmailVerification = () => import('@/components/auth/AuthEmailVerification')

const LayoutBlogList = () => import('@/layouts/LayoutBlogList')
const LayoutBlogPost = () => import('@/layouts/LayoutBlogPost')
const LayoutBlogPostPattern = () => import('@/layouts/LayoutBlogPostPattern')

Vue.use(Router)

export default new Router({
  mode: 'history',
  // beforeEnter: routerGuard,
  routes: [{
      path: '*',
      redirect: { name: 'blog' }
    }, {
      path: '/auth',
      name: 'auth',
      component: LayoutAuth,
      redirect: { name: 'login' },
      children: [{
        path: 'login',
        name: 'login',
        component: AuthLogin
    }, {
        path: 'sign-up',
        name: 'sign-up',
        component: AuthSignup
    }, {
        path: 'forgot-password',
        name: 'forgot-password',
        component: AuthForgotPassword
    }, {
        path: 'password-reset/:hash',
        name: 'password-reset',
        component: AuthPasswordReset
    }, {
        path: 'email-verification/:hash',
        name: 'email-verification',
        component: AuthEmailVerification
    }]
  }, {
      path: '/blog',
      name: 'blog',
      component: LayoutBlogList,
  }, {
      path: '/post/:id',
      name: 'post',
      beforeEnter: routerGuard,
      component: LayoutBlogPost
  }, {
      path: '/post-pattern/:id?',
      name: 'post-pattern',
      beforeEnter: routerGuard,
      component: LayoutBlogPostPattern
  }
]
})