import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as types from '@/store/mutation-types'

import router from '@/router'

Vue.use(Vuex)

const state = {

  // Blog
  allPosts: null,
  selectedPost: null,
  filters: { page: null, user: null, recent: true, keyword: null },

  // Interface
  loading: false
}

const getters = {
  allPosts: state => state.allPosts,
  selectedPost: state => state.selectedPost,
  filters: state => state.filters
}

const actions = {
  getAllPosts({ commit, getters }) {

    const filters = getters.filters

    axios.get('/api/blog-post', { params: filters })
      .then(response => {
        if (response.data.success)
          commit(types.SAVE_ALL_POSTS, response.data)
        else
          commit(types.SAVE_ALL_POSTS, { posts: [], pages: 0 })

      })

  },
  getPostById({ commit }, id) {

    commit(types.SAVE_SELECTED_POST, null)

    axios.get(`/api/blog-post/${id}`)
      .then(response => {
        if (response.data.success)
          commit(types.SAVE_SELECTED_POST, response.data.post)
        else
          router.push({ name: 'blog' })
      })

  },

  createPost({}, form) {

    axios.post('/api/blog-post', form)
      .then(response => {
        if (response.data.success) {}
      })

  },
  updatePost({}, { id, form }) {

    axios.patch(`/api/blog-post/${id}`, form)
      .then(response => {
        if (response.data.success) {}
      })

  },
  deletePost({ dispatch }, id) {

    axios.delete(`/api/blog-post/${id}`)
      .then(response => {
        if (response.data.success) {
          dispatch('getAllPosts')
          router.push({ name: 'blog' })
        }
      })

  },

  saveFilters({ commit, dispatch }, filters) {

    // const queries = router.history.current.query
    router.push({ query: { ...router.history.current.query, ...filters } })

    // If we do not change the page, then we make page number one.
    if ('page' in filters) commit(types.SAVE_FILTERS, filters)
    else {
      router.push({ query: { ...router.history.current.query, page: 1 } })
      commit(types.SAVE_FILTERS, { ...filters, page: 1 })
    }

    dispatch('getAllPosts')
    document.body.scrollTop = document.documentElement.scrollTop = 0

  },
  removeFilters({ commit, dispatch, getters }) {

    if (!Object.keys(router.history.current.query).length) return

    router.replace({ query: {} })

    commit(types.REMOVE_FILTERS)

    dispatch('getAllPosts')
    document.body.scrollTop = document.documentElement.scrollTop = 0

  }
}

const mutations = {
  [types.SAVE_ALL_POSTS](state, posts) { state.allPosts = posts },
  [types.SAVE_SELECTED_POST](state, post) { state.selectedPost = post },

  [types.SAVE_FILTERS](state, filters) {

    Object.keys(filters).forEach(item => {
      state.filters[item] = filters[item]
    })

  },
  [types.REMOVE_FILTERS](state) {
    state.filters = { page: null, user: null, recent: true, keyword: null }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}