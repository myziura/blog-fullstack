import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import router from '@/router'

Vue.use(Vuex)

const state = {
  // Interface
  loading: false
}

const getters = {}

const actions = {
  createComment({ rootGetters, dispatch }, content) {

    const { _id } = rootGetters['blog/selectedPost']

    axios.post(`/api/blog-comment`, { postId: _id, content })
      .then(response => {
        if (response.data.success)
          dispatch('blog/getPostById', _id, { root: true })
      })

  },

  deleteComment({ rootGetters, dispatch }, id) {

    const { _id } = rootGetters['blog/selectedPost']

    axios.delete(`/api/blog-comment/${id}`)
      .then(response => {
        if (response.data.success)
          dispatch('blog/getPostById', _id, { root: true })
      })

  },

  updateComment({ rootGetters, dispatch }, data) {

    const { _id } = rootGetters['blog/selectedPost']

    axios.patch(`/api/blog-comment/${data.id}`, data)
      .then(response => {
        if (response.data.success)
          dispatch('blog/getPostById', _id, { root: true })
      })

  }
}

const mutations = {}

export default {
  state,
  getters,
  actions,
  mutations
}