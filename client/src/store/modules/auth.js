/* Authorization Vuex module
 * - Registration (registration);
 * - Login (login);
 * - Getting info about user (getUserData);
 * - Logout (logout);
 * - Sending password reset email (sendPasswordResetEmail);
 * - Setting new user password (setNewPassword);
 */

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as types from '@/store/mutation-types'

Vue.use(Vuex)

const state = {

  // User
  isAuthenticated: null,
  token: localStorage.getItem('auth-token') || '',
  user: null,

  // Interface
  loading: false,
};

const getters = {

  // User
  isAuthenticated: state => state.isAuthenticated,
  token: state => state.token,
  user: state => state.user,

  // Interface
  loading: state => state.loading,
};

const actions = {
  checkServer() {

    axios.get('/api/auth/')
      .then(response => {})
  },

  // Basic
  login({ commit, dispatch }, data) {

    axios.post('/api/auth/login', data)
      .then(async response => {
        if (response.data.success) {

          commit(types.SAVE_USER_TOKEN, response.data.token)
          localStorage.setItem('auth-token', response.data.token)

          await dispatch('getUserData')

          Vue.prototype.$vueRouter.push({ name: 'blog' })
        }
      })
  },

  registration({ commit, dispatch }, data) {

    axios.post('/api/auth/register', data)
      .then(response => {
        if (response.data.success) dispatch('login', data)
      })
  },

  getUserData({ commit, dispatch, state }) {

    const token = state.token

    if (token) {

      return axios.get('/api/auth/user')
        .then(response => {
          if (response.data.success)
            commit(types.SAVE_USER_INFO, response.data.user)
        })

    } else dispatch('logout')

  },

  logout({ commit }) {

    const pages = ['grid'];
    const currentPage = Vue.prototype.$vueRouter.history.current.name

    if (pages.includes(currentPage))
      Vue.prototype.$vueRouter.push({ name: 'login' })

    commit(types.DELETE_USER_INFO)
  },

  // Verification by email
  sendVerifyEmail({ commit }) {

    axios.get('/api/auth/send-verify-email')
      .then(response => {})
      .catch(error => {})
  },

  verify({ commit }, hash) {
    return axios.get('/api/auth/verify', { params: { hash } })
  },

  // Password reset
  sendPasswordResetEmail({ commit }, email) {

    axios.post('/api/auth/send-reset-password-email', { email })
      .then(response => {
        if (response.data.success) {}
      })
  },

  setNewPassword({ commit }, data) {

    axios.post('/api/auth/reset-password', data)
      .then(response => {
        if (response.data.success)
          Vue.prototype.$vueRouter.push({ name: 'main' })
      })
  },

};

const mutations = {
  [types.SAVE_LOADING_STATE](state, loading) { state.loading = loading },

  [types.SAVE_USER_TOKEN](state, token) { state.token = token },

  [types.SAVE_USER_INFO](state, info) {
    state.user = info
    state.isAuthenticated = true
  },

  [types.DELETE_USER_INFO](state) {
    state.user = null
    state.isAuthenticated = false
    localStorage.removeItem('auth-token')
  },
};

export default { state, getters, actions, mutations }