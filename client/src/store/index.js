import Vue from 'vue'
import Vuex from 'vuex'

import auth from '@/store/modules/auth'
import blog from '@/store/modules/blog'
import comment from '@/store/modules/comment'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: {
      namespaced: true,
      ...auth
    },

    blog: {
      namespaced: true,
      ...blog
    },

    comment: {
      namespaced: true,
      ...comment
    }
  }
})